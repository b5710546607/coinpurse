package coinpurse;
/**
 * AbstractValuable is abstract class that contain compareTo and equals .
 * @author Wanchanapon Thanwaranurak
 */
public abstract class AbstractValuable implements Valuable {
    /** value of the Valuable */
	private double value ;
	private String currency ;
	/** 
	 * Constructor for a new value. 
	 * @param value is the value for the Valuable
	 */
	public AbstractValuable(double value,String currency){
		this.value = value ;
		this.currency = currency ;
	}
	/** 
	 * To compare the value of other Valuable more or less than value of this Valuable
	 * @param obj is the object to compare value
	 * @return result of comparison between Valuable
	 */
	public int compareTo(Valuable value) {
		return (this.getValue()+"").compareTo(value.getValue()+"");
	}
	/** 
	 * To check value other Valuable equal this Valuable.
	 * @param arg is the object to check equal value.
	 * @return true when same Valuable 
	 */
	public boolean equals(Object obj){
		if(obj != null)
			if(obj.getClass() == this.getClass() )
				if(this.getValue() == ((AbstractValuable)obj).getValue())
					return true ;
		return false ;
	}
	/** 
	 * to get the value of this Valuable.
	 * @return value of Valuable.
	 */
	public double getValue(){
		return this.value;
	}
}

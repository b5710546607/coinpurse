
package coinpurse;

import java.util.ResourceBundle;

public abstract class MoneyFactory {
	private static MoneyFactory instance = null ;
	protected MoneyFactory(){
		instance = this ;
	}
	static MoneyFactory getInstance() {
		if(instance==null) 
			setMoneyFactory();
		return instance ;
	}
	public static void setMoneyFactory(){
		ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
		String classfactory = bundle.getString( "moneyfactory" );  
		try {
			instance = (MoneyFactory)Class.forName(classfactory).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
			instance = new MalaiMoneyFactory();
		}
	}
	abstract public Valuable createMoney(double value);
	public Valuable createMoney(String value){
		return createMoney(Double.parseDouble(value));
	}

}



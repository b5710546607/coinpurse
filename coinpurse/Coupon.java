package coinpurse;

import java.util.HashMap;
import java.util.Map;
/**
 * A coupon with a monetary value.
 * You can't change the value of a coupon.
 * @author Wanchanapon Thanwaranurak
 */
public class Coupon extends AbstractValuable {
	/** Name of the color */
	private String color ;
	static Map <String,Double> map = new HashMap <String,Double> (){{
		put("red", 100.0);
		put("blue", 50.0);
		put("green", 20.0);
	}};
			
	/** 
	 * Constructor for a new color. 
	 * @param color is the color, type is String
	 */
	public Coupon(String color){
		super(map.get(color.toLowerCase()));
	}	
	
	/** 
	 * To print the color of this coupon
	 * @return a string of color with coupon  
	 */
	public String toString(){
		return this.color+" coupon";
	}

}

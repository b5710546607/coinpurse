package coinpurse;

import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * Graphic user interface to show the status of item in purse.
 * @author Wanchanapon Thanwaranurak
 *
 */
public class PurseStatusObserver extends JFrame implements Observer{
	/**
	 * the label to show status.
	 */
	private JLabel labelStatus ;
	/**
	 * ProgressBar to show bar by percent of item.
	 */
	private JProgressBar progressbar;
	/**
	 * Constructor for PurseStatusObserver.
	 */
	public PurseStatusObserver(){
		super.setTitle("Purse Status");
		setBounds(180,300,800,600);
		super.setResizable(false);
		super.setSize(200, 60);
		this.initComponets();
		
	}
	/**
	 * Componets for GUI.
	 */
	public void initComponets(){
		JPanel paneStatus = new JPanel();
		paneStatus.setLayout(new BoxLayout (paneStatus,BoxLayout.Y_AXIS));
		labelStatus = new JLabel ("EMPTY",JLabel.CENTER);
		progressbar = new JProgressBar();
		paneStatus.add(labelStatus);
		paneStatus.add(progressbar);
		super.add(paneStatus);
	}
	/**
	 * run this GUI.
	 */
	public void run() {
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	/**
	 * to update the status of this purse.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse) subject ;
			progressbar.setMaximum(purse.getCapacity()); 
			progressbar.setValue(purse.count());
			if(purse.count()==0){
				labelStatus.setText("EMPTY");
			}
			else if(purse.isFull()){
				labelStatus.setText("FULL");
			}
			else
				labelStatus.setText(purse.count()+"");
		}
		if( info != null) System.out.println( info );
	}
	
}

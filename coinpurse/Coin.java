package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Wanchanapon Thanwaranurak
 */
public class Coin extends AbstractValuable{
	private double value ;
	private String currency ;
	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value , String currency) {
		super(value,currency);
		this.value = value ;
		this.currency = currency ;	
	}
	/** 
	 * To print the value of this coin
	 * @return a string of value with Baht  
	 */
	public String toString(){
		return (int)super.getValue()+"-"+this.currency+" coin" ;
	}
}

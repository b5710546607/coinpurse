package coinpurse;
/**
 * A BankNote with a monetary value.
 * You can't change the value of a BankNote.
 * @author Wanchanapon Thanwaranurak
 */
public class BankNote extends AbstractValuable {
	/** IdBankNote of the serialNumber  */
	private int serialNumber ;
	private double value ;
	private String currency ;
	/** Next IdBankNote of the nextSerialNumber  */
	private static int nextSerialNumber = 1000000 ;
	/** 
	 * Constructor for a new BankNote. 
	 * @param value is the value for the BankNote
	 */
	public BankNote(double value,String currency){
		super(value, currency) ;
		this.value = value ;
		this.currency = currency ;	
		this.serialNumber = nextSerialNumber ;
		nextSerialNumber++;
	}
	/** 
	 * to get the Next IdBankNote of this BankNote.
	 * @return Next IdBankNote of BankNote.
	 */
	public static int getNextSerialNumber() {
		return  nextSerialNumber ;
	}
	/** 
	 * To print the value and the IdBankNote of this BankNote
	 * @return a string of value and idBankNote with BankNote  
	 */
	public String toString(){
		return String.format("%.0f-%s Banknote [%d]",this.value,this.currency,this.serialNumber) ;
	}
}

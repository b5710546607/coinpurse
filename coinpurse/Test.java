package coinpurse;
import java.util.Arrays;

public class Test {
	public static void main(String[] args) {
		//                Purse purse = new Purse( 3 );
		//                System.out.println(purse.getBalance( )) ;//     returns 0.0       (nothing in Purse yet)
		//                System.out.println(purse.count( ));//   returns 0
		//                System.out.println(purse.isFull( ))     ;//returns false
		//                System.out.println(purse.insert(new Coin(5)));//        returns true
		//                System.out.println(purse.insert(new Coin(10)))  ;// returns true
		//                System.out.println(purse.insert(new Coin(0)));//        returns false. Don't allow coins with value <= 0.
		//                System.out.println(purse.insert(new Coin(5))) ;//       returns true
		//                System.out.println(purse.insert(new Coin(5)) );//       returns false because purse is full (capacity 3 coins)
		//                System.out.println(purse.count( ));//   returns 3
		//                System.out.println(purse.isFull( ))     ;//returns true
		//                System.out.println(purse.getBalance( ));//      returns 20.0
		//                System.out.println(purse.toString());// returns "3 coins with value 20.0", or
		//                        //      "2 5-Baht coin, 1 10-Baht coin"
		//                System.out.println(purse.withdraw(11));//       returns null.  Can't withdraw exactly 11 Baht.
		//                System.out.println(Arrays.toString(purse.withdraw(15)));//       returns an array:  [ Coin(10), Coin(5) ]
		//                System.out.println(purse.getBalance());//       returns 5.
		MoneyFactory factory = MoneyFactory.getInstance();
		System.out.println( factory.createMoney("10") );
		System.out.println( factory.createMoney(0.05) );



	}

}
package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;


/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

	/**
	 * @param args not used
	 */
	public static void main( String[] args ) {
		Purse purse = new Purse(10);
		PurseBalanceObserver balance = new PurseBalanceObserver();
		balance.run();
		PurseStatusObserver status = new PurseStatusObserver();
		status.run();
		purse.addObserver(balance);
		purse.addObserver(status);
		purse.setWithdrawStrategy(new RecursiveWithdraw());
		ConsoleDialog console = new ConsoleDialog (purse);
		console.run();
	}
}

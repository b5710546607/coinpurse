package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

/**
 * A WithdrawStrategy is interface which contain withdraw method.
 * @author Wanchanapon Thanwaranurak
 */
public interface WithdrawStrategy {
	/** 
	 * withdraw money from Purse.
	 * @param amount of the value that want to withdraw.
	 * @param list of available valuable.
	 * @return array of Valuable objects for money withdraw.
	 */
	public Valuable[] withdraw (double amount , List<Valuable> valuable);
}

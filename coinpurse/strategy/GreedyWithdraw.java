package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 *  A GreedyWithdraw is withdraw the valuable with greedy algorithm.
 *  
 *  @author Wanchanapon Thanwaranurak
 */
public class GreedyWithdraw implements WithdrawStrategy{
	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Valuable withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @param list of available valuable.
	 *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount , List<Valuable> valuable) {
		/*
		 * One solution is to start from the most valuable money
		 * in the purse and take any money that maybe used for
		 * withdraw.
		 * Since you don't know if withdraw is going to succeed, 
		 * don't actually withdraw the money from the purse yet.
		 * Instead, create a temporary list.
		 * Each time you see a coin that you want to withdraw,
		 * add it to the temporary list and deduct the value
		 * from amount. (This is called a "Greedy Algorithm".)
		 * Or, if you don't like changing the amount parameter,
		 * use a local total to keep track of amount withdrawn so far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), 
		 * then you are done.
		 * Now you can withdraw the money from the purse.
		 * NOTE: Don't use list.removeAll(templist) for this
		 * becuase removeAll removes *all* coins from list that
		 * are equal (using Valuable.equals) to something in templist.
		 * Instead, use a loop over templist
		 * and remove coins one-by-one.		
		 */


		Collections.sort(valuable ,new ValueComparator());
		ArrayList MoneyWithdraw = new ArrayList<Valuable>();
		ArrayList index = new ArrayList();
		if ( amount > 0 )
		{	
			double balance = 0 ;
			for(int i = 0 ; i < valuable.size() ; i++){
				balance += valuable.get(i).getValue();
			}
			if(amount>balance){
				return null;
			}
			else{
				for(int i = 0 ; i < valuable.size() ; i++){
					if(amount >= valuable.get(i).getValue()){
						amount -= valuable.get(i).getValue();
						MoneyWithdraw.add(valuable.get(i));
						index.add(i);					
					}	
				}
			}
			if(amount!= 0){
				return null;
			}
			else{
				for(int i = index.size()-1 ; i >= 0 ; i--){
					valuable.remove((int)index.get(i));			
				}
			}
		}
		Valuable coin[] = new Valuable [MoneyWithdraw.size()];
		for (int i = 0 ; i < MoneyWithdraw.size() ; i++){
			coin[i] = (Valuable)MoneyWithdraw.get(i);
		}
		return coin ;
	}
}

package coinpurse.strategy;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * A RecursiveWithdraw is withdraw the valuable with greedy algorithm.
 * @author Wanchanapon Thanwaranurak
 */
public class RecursiveWithdraw implements WithdrawStrategy{

	/** 
	 * withdraw money from Purse.
	 * @param amount of the value that want to withdraw.
	 * @param list of available valuable.
	 * @return array of Valuable objects for money withdraw.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> list) {
		Collections.sort(list ,new ValueComparator());
		List<Valuable> recursive =  withdrawFrom(amount,list,list.size()-1) ;
		Valuable[] withdraw = null ;
		if(recursive != null){
		withdraw = new Valuable [recursive.size()] ;
			for(int i = 0 ; i < withdraw.length ; i++){
				withdraw[i] = recursive.get(i) ;
			}
		}
		return  withdraw ; 
	}
	/** 
	 * help method withdraw ,withdraw money from Purse.
	 * @param amount of the value that want to withdraw.
	 * @param list of available valuable.
	 * @param index is the value of list.
	 * @return ArrayList of Valuable objects for money withdraw.
	 */
	public List<Valuable> withdrawFrom (double amount, List<Valuable> list , int index){
		if(index<0){
			return null ;
		}
		if(amount<=0){
			return null ;
		}
		if(amount-list.get(index).getValue()==0){
			List<Valuable>  store = new ArrayList<Valuable>();
			store.add(list.get(index));
			return store ;
		}
		List<Valuable>  recursive =  withdrawFrom( amount-list.get(index).getValue(), list , index-1);	
		if(recursive == null){
			recursive = withdrawFrom( amount, list , index-1);	
		}
		else{
			recursive.add(list.get(index));
		}
		return recursive ;
	}
}


package coinpurse;

import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * Graphic user interface to show the balance of item in purse.
 * @author Wanchanapon Thanwaranurak
 *
 */
public class PurseBalanceObserver extends JFrame implements Observer{
	/**
	 * the label to show balance.
	 */
	private JLabel labelBalance ;
	/**
	 * Constructor for PurseBalanceObserver.
	 */
	public PurseBalanceObserver(){
		super.setTitle("Purse Balance");
		setBounds(180,100,800,600);
		super.setResizable(false);
		super.setSize(200, 60);
		this.initComponets();

	}
	/**
	 * Componets for GUI.
	 */
	public void initComponets(){
		JPanel paneBalance = new JPanel();
		labelBalance = new JLabel ("0 Baht",JLabel.CENTER);
		paneBalance.add(labelBalance);
		super.add(paneBalance);
	}
	/**
	 * run this GUI.
	 */
	public void run() {
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	/**
	 * to update the balance of this purse.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse) subject ;
			labelBalance.setText(purse.getBalance()+" Baht");
		}
		if( info != null) System.out.println( info );
	}


}

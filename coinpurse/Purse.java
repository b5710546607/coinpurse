package coinpurse;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A coin purse contains money.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the money purse decides which
 *  money to remove.
 *  
 *  @author Wanchanapon Thanwaranurak
 */
public class Purse extends Observable{
	/** Collection of money in the purse. */
	private List<Valuable> money;
	/** Capacity is maximum NUMBER of money the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;
	/** 
	 * withdraw with differ strategy.
	 */
	private WithdrawStrategy strategy;
	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of money you can put in purse.
	 */
	public Purse( int capacity ) {
		money = new ArrayList<Valuable>();
		this.capacity = capacity ;
		setWithdrawStrategy(new RecursiveWithdraw());
	}

	/**
	 * Count and return the number of money in the purse.
	 * This is the number of coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() { 
		return this.money.size(); 
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0 ;
		for(int i = 0 ; i < money.size() ; i++){
			balance += money.get(i).getValue();
		}
		return balance; 
	}
	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */
	public int getCapacity() { 
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		if(this.money.size()==this.capacity)
			return true ;
		return false;
	}

	/** 
	 * Insert a money into the purse.
	 * The money is only inserted if the purse has space for it
	 * and the money has positive value.  No worthless coins!
	 * @param money is a Valuable object to insert into purse
	 * @return true if money inserted, false if can't insert
	 */
	public boolean insert( Valuable coin ) {
		// if the purse is already full then can't insert anything.
		if(coin.getValue() > 0){
			if(isFull() == false){
				money.add(coin);
				super.setChanged();
				super.notifyObservers(this);
				return true;
			}
		}
		return false ;
	}
	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Valuable withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		
		Valuable[] temp = strategy.withdraw(amount,money);
		double withdrewAmount = 0;
		if(temp != null){
			for(int i = 0; i < temp.length ;i++) {
				withdrewAmount += temp[i].getValue();
			}
			if (temp.length >0 && amount-withdrewAmount == 0) {
				for(int i = 0;i< temp.length ;i++){
					money.remove( temp[i]);
				}
				Valuable[] withdrawCoins = new Valuable[temp.length];
				for(int i = 0;i<temp.length;i++){
					withdrawCoins[i] = temp[i];
				}
				super.setChanged();
				super.notifyObservers(this);
				return withdrawCoins;
			}			
		}
		
		return null;
	}
	/**
	 * set new strategy type.
	 * @param strategy type that want to set.
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy = strategy ;
	}
	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 */
	public String toString() {
		return money.size()+" money with value "+getBalance();
	}
}


package coinpurse;

import java.util.Comparator;
/**
 * A ValueComparator a Comparable with Valuable.
 * @author Wanchanapon Thanwaranurak
 */
public class ValueComparator implements Comparator<Valuable>{
	/** 
	 * to get the value of method.
	 * @return value that type int.
	 */
	public int compare(Valuable a, Valuable b) {
		 if (a.getValue() > b.getValue()) {
	           return -1;
	       } else if (a.getValue() < b.getValue()){
	           return 1;
	       } else {
	           return 0;
	       }
	}

}

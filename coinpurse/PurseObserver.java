package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
/**
 * Observes the Purse.
 * @author Wanchanapon Thanwaranurak
 *
 */
public class PurseObserver implements Observer{
	/**
	 * it constructor.
	 */
	public PurseObserver(){

	}
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse) subject ;
			int balance = (int) purse.getBalance();
			System.out.println("Balance is: "+balance);
		}
		if( info != null) System.out.println( info );
	}

}

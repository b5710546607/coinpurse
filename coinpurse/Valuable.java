package coinpurse;
/**
 * A Valuable with interface of Coin,Coupon,BankNote.
 * @author Wanchanapon Thanwaranurak
 */
public interface Valuable extends Comparable<Valuable> {
	/** 
	 * to get the value of method.
	 * @return value that type double.
	 */
	public double getValue();
}
